# Employing Temporal Information in Deep Learning of Video Captioning #

Used concepts from the paper ["Consensus-based Sequence Training for Video Captioning" (Phan, Henter, Miyao, Satoh. 2017)](https://arxiv.org/abs/1712.09532).

## Dependencies ###

* Python 2.7
* Pytorch 0.4
* [Microsoft COCO Caption Evaluation](https://github.com/tylin/coco-caption)
* [CIDEr](https://github.com/plsang/cider)

(Check out the `coco-caption` and `cider` projects into your working directory)

![Dataset and Evaluation Metrics](https://bitbucket.org/rabkinda/buffervideocaptioning/raw/master/4README/Dataset_and_Metrics.PNG)

![Methodology Visualization](https://bitbucket.org/rabkinda/buffervideocaptioning/raw/master/4README/captioning_methodology_visualization.PNG)
![Methodology](https://bitbucket.org/rabkinda/buffervideocaptioning/raw/master/4README/Captioning_methodology_step1.PNG)
![Methodology](https://bitbucket.org/rabkinda/buffervideocaptioning/raw/master/4README/captioning_methodology_step3.PNG)
![Methodology](https://bitbucket.org/rabkinda/buffervideocaptioning/raw/master/4README/captioning_methodology_step4.PNG)

![Run Options](https://bitbucket.org/rabkinda/buffervideocaptioning/raw/master/4README/run_options.PNG)
![Results](https://bitbucket.org/rabkinda/buffervideocaptioning/raw/master/4README/results.PNG)


### Acknowledgements ###

* Torch implementation of [NeuralTalk2](https://github.com/karpathy/neuraltalk2)
* PyTorch implementation of Self-critical Sequence Training for Image Captioning [(SCST)](https://github.com/ruotianluo/self-critical.pytorch)
* PyTorch Team
