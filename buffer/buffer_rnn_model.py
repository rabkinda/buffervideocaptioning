# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import inspect
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


def getLinear(dim_in, dim_out, mul_ratio, bias, use_dropout, dropout_prob, buffer_stop_after_first_fc, use_bn):
    hidden_layer_size = int(mul_ratio*dim_out) if not buffer_stop_after_first_fc else dim_out
    modules = nn.Sequential()
    i=0

    if use_dropout:
        modules.add_module(name=str(i), module=nn.Dropout(p=dropout_prob))
        i+=1

    modules.add_module(name=str(i), module=nn.Linear(dim_in, hidden_layer_size, bias))
    i+=1

    # if use_bn:
    #     modules.add_module(name=str(i), module=nn.BatchNorm1d(hidden_layer_size))
    #     i += 1

    modules.add_module(name=str(i), module=nn.Tanh())
    i+=1

    if not buffer_stop_after_first_fc:
        modules.add_module(name=str(i), module=nn.Linear(hidden_layer_size, dim_out, bias))
        i+=1

        # if use_bn:
        #     modules.add_module(name=str(i), module=nn.BatchNorm1d(dim_out))
        #     i += 1

        modules.add_module(name=str(i), module=nn.Tanh())

    return modules


def getResetGateNetwork(dim_in, bias, use_dropout, dropout_prob, buffer_reset_network_type, mem_size_out, mem_feat_size_out, input_size_out, use_bn,
                        use_input_in_reset_gate, use_buffer_in_reset_gate):
    dim_out = 0
    modules = nn.Sequential()
    i=0

    if use_dropout:
        modules.add_module(name=str(i), module=nn.Dropout(p=dropout_prob))
        i+=1

    if buffer_reset_network_type == 'all':
        dim_out = dim_in
    elif buffer_reset_network_type == 'separate':
        dim_out = mem_size_out+mem_feat_size_out+input_size_out

    if not use_input_in_reset_gate:
        dim_out-=input_size_out

    if not use_buffer_in_reset_gate:
        dim_out = input_size_out

    modules.add_module(name=str(i), module=nn.Linear(dim_in, dim_out, bias))
    i+=1

    # if use_bn:
    #     modules.add_module(name=str(i), module=nn.BatchNorm1d(dim_out))
    #     i += 1

    #modules.add_module(name=str(i), module=nn.Sigmoid())
    return modules


class LoopRNN(nn.Module):
    def __init__(self, input_size, hidden_size, mem_size, buffer_mul_ratio, bias=1,
                 buffer_dropout_N_u_prob=0, buffer_dropout_N_o_prob=0, buffer_input_reduce_first=0, noise=10.0,
                 use_reset_gate=1, buffer_reset_gate_dropout=0, buffer_stop_after_first_fc=0, buffer_save_reset_gate_in_memory=1,
                 buffer_reset_network_type='all', buffer_use_different_reset_gates=0, buffer_use_bn=0, use_input_in_reset_gate=1,
                 use_buffer_in_reset_gate=1):
        super(LoopRNN, self).__init__()

        print([(arg, locals()[arg]) for arg in inspect.getargspec(LoopRNN.__init__).args[1:]])#print buffer arguments

        self.buffer_input_reduce_first = buffer_input_reduce_first
        self.noise_std = noise
        self.use_reset_gate = use_reset_gate
        self.mem_size = mem_size
        self.buffer_save_reset_gate_in_memory = buffer_save_reset_gate_in_memory
        self.buffer_reset_network_type = buffer_reset_network_type
        self.mem_feat_size = hidden_size
        self.final_input_size = hidden_size if self.buffer_input_reduce_first else input_size
        self.buffer_use_different_reset_gates = buffer_use_different_reset_gates
        self.use_input_in_reset_gate = use_input_in_reset_gate
        self.use_buffer_in_reset_gate = use_buffer_in_reset_gate
        mem_elem = mem_size * self.mem_feat_size

        N_u_input_size = (mem_elem-self.mem_feat_size)+self.final_input_size
        self.N_u = getLinear(N_u_input_size, self.mem_feat_size, buffer_mul_ratio, bias, True, buffer_dropout_N_u_prob,
                             buffer_stop_after_first_fc, buffer_use_bn)
        self.N_o = getLinear(mem_elem, hidden_size, buffer_mul_ratio, bias, True, buffer_dropout_N_o_prob,
                             buffer_stop_after_first_fc, buffer_use_bn)

        if self.use_reset_gate:
            self.Nu_reset_gate = getResetGateNetwork(N_u_input_size, bias, True, buffer_reset_gate_dropout, self.buffer_reset_network_type, mem_size - 1,
                                                     self.mem_feat_size, self.final_input_size, buffer_use_bn, self.use_input_in_reset_gate,
                                                     self.use_buffer_in_reset_gate)
            if self.buffer_use_different_reset_gates:
                self.No_reset_gate = getResetGateNetwork(mem_elem, bias, True, buffer_reset_gate_dropout, self.buffer_reset_network_type, mem_size,
                                                         self.mem_feat_size, 0, buffer_use_bn, self.use_input_in_reset_gate, self.use_buffer_in_reset_gate)

        if self.buffer_input_reduce_first:
            self.i2h_reset_gate = nn.Linear(input_size, hidden_size, bias=bias)
            self.i2h_Nu_No = nn.Linear(input_size, hidden_size, bias=bias)

        self.video_dsc_std = 0.33
        self.otm1_dsc_std = 0.07


    def cuda(self, device_id=None):
        nn.Module.cuda(self, device_id)


    def update_buffer(self, input, S_tm1):
        #apply noise to input
        if self.training and self.noise_std > 0:

            otm1_dsc_noise_tensor = torch.cuda.FloatTensor(input.size(0), 512).fill_(0)\
                .normal_(0, (self.noise_std/100.0) * self.otm1_dsc_std)
            video_dsc_noise_tensor = torch.cuda.FloatTensor(input.size(0), input.size(1)-512).fill_(0) \
                .normal_(0, (self.noise_std / 100.0) * self.video_dsc_std)
            noise_tensor = torch.cat([otm1_dsc_noise_tensor, video_dsc_noise_tensor], 1)
            input = input+Variable(noise_tensor, requires_grad=False)

        if self.buffer_input_reduce_first:
            input_for_reset_gate = self.i2h_reset_gate(input)
            input = self.i2h_Nu_No(input)
        else:
            input_for_reset_gate = input

        Sp = S_tm1[:, :, :-1].contiguous()
        flatten_Sp = Sp.view(Sp.size(0), -1)
        Sp_for_Nu_No = torch.cat([input, flatten_Sp], 1)

        if self.use_reset_gate:
            Sp_for_reset_gate = torch.cat([input_for_reset_gate, Sp.view(Sp.size(0), -1)], 1)
            reset_gate = self.apply_reset_gate(Sp_for_reset_gate, self.Nu_reset_gate, self.mem_size - 1, self.mem_feat_size, self.final_input_size)
            if not self.use_input_in_reset_gate:
                reset_gate = torch.cat([Variable(torch.cuda.FloatTensor(input_for_reset_gate.size()).fill_(1), requires_grad=False) , reset_gate], 1)
            elif not self.use_buffer_in_reset_gate:
                reset_gate = torch.cat([reset_gate, Variable(torch.cuda.FloatTensor(flatten_Sp.size()).fill_(1), requires_grad=False)], 1)
            Sp_for_Nu_No = Sp_for_Nu_No * reset_gate

        # update S
        u = self.N_u(Sp_for_Nu_No)
        u = u.unsqueeze(2)
        S_after_reset_gate = torch.cat([u, Sp_for_Nu_No[:, input.size(1):].contiguous().view(u.size(0), u.size(1), self.mem_size-1)], 2)

        if self.buffer_save_reset_gate_in_memory:
            S = S_after_reset_gate
        else:
            S = torch.cat([u, S_tm1[:, :, :-1]], 2)

        return S, S_after_reset_gate


    def apply_reset_gate(self, Sp, N_reset_gate, mem_size_out, mem_feat_size_out, input_size_out):
        reset_gate = N_reset_gate(Sp)

        if self.buffer_reset_network_type == 'separate':
            reset_gate = self.handle_separate_reset_gate(reset_gate, mem_size_out, mem_feat_size_out, input_size_out,
                                                         self.use_input_in_reset_gate, self.use_buffer_in_reset_gate)

        reset_gate = F.sigmoid(reset_gate)
        return reset_gate


    def handle_separate_reset_gate(self, reset_gate, mem_size_out, mem_feat_size_out, input_size_out, use_input_in_reset_gate,
                                   use_buffer_in_reset_gate):
        curr_ind = 0

        if use_buffer_in_reset_gate:
            mem_cell_reset_gate = reset_gate[:, curr_ind:curr_ind+mem_size_out]
            curr_ind += mem_size_out
            mem_feat_reset_gate = reset_gate[:, curr_ind:curr_ind+mem_feat_size_out]
            curr_ind += mem_feat_size_out

            mem_cell_reset_gate = mem_cell_reset_gate.unsqueeze(1).repeat(1, mem_feat_size_out, 1)
            mem_feat_reset_gate = mem_feat_reset_gate.unsqueeze(2).repeat(1, 1, mem_size_out)
            buffer_reset_gate = mem_cell_reset_gate * mem_feat_reset_gate
            buffer_reset_gate = buffer_reset_gate.view(buffer_reset_gate.size(0), -1)

        if use_input_in_reset_gate and input_size_out>0:
            input_reset_gate = reset_gate[:, curr_ind:curr_ind+input_size_out]

        if use_buffer_in_reset_gate and use_input_in_reset_gate and input_size_out > 0:
            return torch.cat([input_reset_gate, buffer_reset_gate], 1)
        elif use_buffer_in_reset_gate:
            return buffer_reset_gate
        else:
            return input_reset_gate


    def get_output_by_buffer(self, S_t, S_t_after_reset_gate):
        if self.use_reset_gate:
            if self.buffer_use_different_reset_gates:
                Sp = S_t.view(S_t.size(0), -1)
                reset_gate = self.apply_reset_gate(Sp, self.No_reset_gate, self.mem_size, self.mem_feat_size, 0)
                Sp = Sp * reset_gate
            else:
                Sp = S_t_after_reset_gate.view(S_t_after_reset_gate.size(0), -1)
        else:
            Sp = S_t.view(S_t.size(0), -1)

        o_t = self.N_o(Sp)
        return o_t


    def forward(self, input, state):
        seq_len = input.size(0)
        S_t = state.permute(1, 2, 0)
        out = []

        for seq_ind in range(seq_len):
            curr_input = input[seq_ind]
            # update buffer
            S_t, S_t_after_reset_gate = self.update_buffer(curr_input, S_t)

            # predict next time step based on buffer content
            o_t = self.get_output_by_buffer(S_t, S_t_after_reset_gate)
            out += [o_t]

        out_seq = torch.stack(out)
        S_t = S_t.permute(2, 0, 1)
        return out_seq, S_t
