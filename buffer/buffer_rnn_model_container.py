# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import numpy as np
import torch
import torch.nn as nn
from buffer_rnn_model import LoopRNN


class LoopRNNContainer(nn.Module):
    def __init__(self, buffer_short_size=3, buffer_long_size=5, **kargs):
        super(LoopRNNContainer, self).__init__()

        self.short_buffer = LoopRNN(mem_size=buffer_short_size, **kargs)
        self.long_buffer = LoopRNN(mem_size=buffer_long_size, **kargs)

        # self.merger_net = nn.Sequential(nn.Dropout(p=kargs['buffer_dropout_N_o_prob']),
        #                                 nn.Linear((2*kargs['hidden_size'])+kargs['input_size'], kargs['hidden_size'], bias=kargs['bias']),
        #                                 nn.Tanh())
        #self.merger_net = nn.Sequential(nn.Linear(kargs['hidden_size'], kargs['hidden_size'], bias=kargs['bias']),
        #                                 nn.Tanh())

    def cuda(self, device_id=None):
        nn.Module.cuda(self, device_id)


    def forward(self, input, state):
        short_range_candidate, short_buffer_state = self.short_buffer(input, state[0])
        long_range_candidate, long_buffer_state = self.long_buffer(input, state[1])

        # combined_candidate = self.merger_net(torch.cat([input.squeeze(0), short_range_candidate.squeeze(0), long_range_candidate.squeeze(0)], dim=1)).unsqueeze(0)
        combined_candidate = short_range_candidate + long_range_candidate
        #combined_candidate = self.merger_net(combined_candidate)
        return combined_candidate, (short_buffer_state, long_buffer_state)
