import argparse
import numpy as np
import matplotlib.pyplot as plt
import torch
from matplotlib.ticker import MaxNLocator
import os
#from buffer.buffer_rnn_model import LoopRNN


def sigmoid(z):
    g = 1 / (1 + np.exp(-z))
    return g

parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--checkpoint', type=str, help='checkpoint')
args = parser.parse_args()

weights = torch.load(args.checkpoint, map_location=lambda storage, loc: storage)
opt = weights['opt']
# model = LoopRNN(opt.input_encoding_size + opt.video_encoding_size, opt.rnn_size, opt.buffer_mem_size, opt.buffer_mul_ratio, 0,
#                 opt.buffer_dropout_N_u_prob, opt.buffer_dropout_N_o_prob, opt.buffer_input_reduce_first, opt.buffer_noise,
#                 opt.buffer_use_reset_gate, opt.buffer_reset_gate_dropout, opt.buffer_stop_after_first_fc, opt.buffer_save_reset_gate_in_memory,
#                 opt.buffer_reset_network_type, opt.buffer_use_different_reset_gates, 0)

buffer_weights = {k.replace('module.core.rnn.',''):v for k,v in weights['model'].iteritems() if 'module.core.rnn.' in k}
del weights
#model.load_state_dict(buffer_weights)

# N_reset_gate_fc_display = np.mean(np.abs(buffer_weights['Nu_reset_gate.1.weight'].numpy()), 0)
# N_reset_gate_fc_display = np.append(np.mean(N_reset_gate_fc_display[:5*512]),
#                                     np.mean(N_reset_gate_fc_display[5*512:].reshape(opt.rnn_size, opt.buffer_mem_size-1), 0))
N_reset_gate_fc_next_comp = np.mean(np.abs(buffer_weights['Nu_reset_gate.1.weight'].numpy()), 1)
N_reset_gate_fc_next_comp = np.append(np.mean(N_reset_gate_fc_next_comp[:5*512]),
                                      np.mean(N_reset_gate_fc_next_comp[(5 * 512) + opt.buffer_mem_size - 1:])
                                      * N_reset_gate_fc_next_comp[5*512:(5*512)+opt.buffer_mem_size-1])

#N_r_fc = N_reset_gate_fc_display
N_reset_gate_fc_next_comp = sigmoid(N_reset_gate_fc_next_comp)
N_o_fc = np.mean(np.mean(np.abs(buffer_weights['N_o.1.weight'].numpy()), 0).reshape(opt.rnn_size, opt.buffer_mem_size), 0)
N_o_fc = np.append(1, N_reset_gate_fc_next_comp[1:])*N_o_fc
N_u_fc = np.mean(np.abs(buffer_weights['N_u.1.weight'].numpy()), 0)
N_u_fc_input = np.mean(N_u_fc[:5*512])
N_u_fc_buffer = np.mean(N_u_fc[5*512:].reshape(opt.rnn_size, opt.buffer_mem_size-1), 0)
N_u_fc = np.append(N_u_fc_input*N_reset_gate_fc_next_comp[0], N_reset_gate_fc_next_comp[1:]*N_u_fc_buffer)

fig = plt.figure(figsize=(8, 4.25))
ax = fig.add_subplot(111)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))

names = ('N_o', 'N_u')#, 'N_r')

N_plot = np.array(N_o_fc)
plt.plot(np.arange(1, opt.buffer_mem_size+1,1, dtype=np.uint8), N_plot,
         ('-r*' if names[0].startswith('N_o') else '-bo'))

N_plot = np.array(N_u_fc)
plt.plot(np.arange(1, opt.buffer_mem_size+1,1, dtype=np.uint8), N_plot,
         ('-r*' if names[1].startswith('N_o') else '-bo'))

# N_plot = np.array(N_r_fc)
# plt.plot(np.arange(1, opt.buffer_mem_size+1,1, dtype=np.uint8), N_plot,
#          ('-r*' if names[1].startswith('N_o') else '-b*'))

plt.legend(names)

plt.xlim([0.75, opt.buffer_mem_size+0.25])
plt.xlabel('Buffer Position', fontsize=13)
plt.ylabel('Mean Of Absolute Weights', fontsize=13)
plt.grid()
plt.savefig('./results/'+os.path.dirname(args.checkpoint).split(os.sep)[-1]+'_Buffer_Weights.png', dpi=200)
