import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
import os
import utils
from buffer.buffer_rnn_model_container import LoopRNNContainer


def to_contiguous(tensor):
    if tensor.is_contiguous():
        return tensor
    else:
        return tensor.contiguous()


class RewardCriterion(nn.Module):

    def __init__(self):
        super(RewardCriterion, self).__init__()

    def forward(self, seq, logprobs, reward):

        # import pdb; pdb.set_trace()
        logprobs = to_contiguous(logprobs).view(-1)
        reward = to_contiguous(reward).view(-1)
        mask = (seq > 0).float()
        # add one to the right to count for the <eos> token
        mask = to_contiguous(torch.cat(
            [mask.new(mask.size(0), 1).fill_(1), mask[:, :-1]], 1)).view(-1)
        #import pdb; pdb.set_trace()
        output = - logprobs * reward * Variable(mask)
        output = torch.sum(output) / torch.sum(mask)

        return output


class CrossEntropyCriterion(nn.Module):

    def __init__(self):
        super(CrossEntropyCriterion, self).__init__()

    def forward(self, pred, target, mask):
        # truncate to the same size
        target = target[:, :pred.size(1)]
        mask = mask[:, :pred.size(1)]

        pred = to_contiguous(pred).view(-1, pred.size(2))
        target = to_contiguous(target).view(-1, 1)
        mask = to_contiguous(mask).view(-1, 1)

        output = -pred.gather(1, target) * mask
        output = torch.sum(output) / torch.sum(mask)

        return output


class FeatPool(nn.Module):

    def __init__(self, feat_dims, out_size, dropout):
        super(FeatPool, self).__init__()

        module_list = []
        for dim in feat_dims:
            module = nn.Sequential(
                nn.Linear(
                    dim,
                    out_size),
                nn.ReLU(),
                nn.Dropout(dropout))
            module_list += [module]
        self.feat_list = nn.ModuleList(module_list)

        # self.embed = nn.Sequential(nn.Linear(sum(feat_dims), out_size), nn.ReLU(), nn.Dropout(dropout))

    def forward(self, feats):
        """
        feats is a list, each element is a tensor that have size (N x C x F)
        at the moment assuming that C == 1
        """
        out = torch.cat([m(feats[i].squeeze(1))
                         for i, m in enumerate(self.feat_list)], 1)
        # pdb.set_trace()
        # out = self.embed(torch.cat(feats, 2).squeeze(1))
        return out


class FeatExpander(nn.Module):

    def __init__(self, n=1):
        super(FeatExpander, self).__init__()
        self.n = n

    def forward(self, x):
        if self.n == 1:
            out = x
        else:
            out = Variable(
                x.data.new(
                    self.n * x.size(0),
                    x.size(1)),
                volatile=x.volatile)
            for i in range(x.size(0)):
                out[i * self.n:(i + 1) *
                    self.n] = x[i].expand(self.n, x.size(1))
        return out

    def set_n(self, x):
        self.n = x


class RNNUnit(nn.Module):

    def __init__(self, opt):
        super(RNNUnit, self).__init__()
        self.rnn_type = opt.rnn_type
        self.rnn_size = opt.rnn_size
        self.num_layers = opt.num_layers
        self.drop_prob_lm = opt.drop_prob_lm

        if opt.model_type == 'standard':
            self.input_size = opt.input_encoding_size
        elif opt.model_type in ['concat', 'manet']:
            self.input_size = opt.input_encoding_size + opt.video_encoding_size

        if self.rnn_type == 'buffer':
            __import__(opt.loop_model)
            LoopRNN = utils.my_import(opt.loop_model + '.LoopRNN')

            self.rnn = LoopRNN(input_size=self.input_size,
                               hidden_size=self.rnn_size,
                               mem_size=opt.buffer_mem_size,
                               buffer_mul_ratio=opt.buffer_mul_ratio,
                               bias=opt.buffer_use_bias,
                               buffer_dropout_N_u_prob=opt.buffer_dropout_N_u_prob,
                               buffer_dropout_N_o_prob=opt.buffer_dropout_N_o_prob,
                               buffer_input_reduce_first=opt.buffer_input_reduce_first,
                               noise=opt.buffer_noise,
                               use_reset_gate=opt.buffer_use_reset_gate,
                               buffer_reset_gate_dropout=opt.buffer_reset_gate_dropout,
                               buffer_stop_after_first_fc=opt.buffer_stop_after_first_fc,
                               buffer_save_reset_gate_in_memory=opt.buffer_save_reset_gate_in_memory,
                               buffer_reset_network_type=opt.buffer_reset_network_type,
                               buffer_use_different_reset_gates=opt.buffer_use_different_reset_gates,
                               buffer_use_bn=opt.buffer_use_bn,
                               use_input_in_reset_gate=opt.use_input_in_reset_gate,
                               use_buffer_in_reset_gate=opt.use_buffer_in_reset_gate)
        elif self.rnn_type == 'buffer_container':
            self.rnn = LoopRNNContainer(input_size=self.input_size,
                                        hidden_size=self.rnn_size,
                                        buffer_mul_ratio=opt.buffer_mul_ratio,
                                        bias=opt.buffer_use_bias,
                                        buffer_dropout_N_u_prob=opt.buffer_dropout_N_u_prob,
                                        buffer_dropout_N_o_prob=opt.buffer_dropout_N_o_prob,
                                        buffer_input_reduce_first=opt.buffer_input_reduce_first,
                                        noise=opt.buffer_noise,
                                        use_reset_gate=opt.buffer_use_reset_gate,
                                        buffer_reset_gate_dropout=opt.buffer_reset_gate_dropout,
                                        buffer_stop_after_first_fc=opt.buffer_stop_after_first_fc,
                                        buffer_save_reset_gate_in_memory=opt.buffer_save_reset_gate_in_memory,
                                        buffer_reset_network_type=opt.buffer_reset_network_type,
                                        buffer_use_different_reset_gates=opt.buffer_use_different_reset_gates,
                                        buffer_use_bn=opt.buffer_use_bn,
                                        use_input_in_reset_gate=opt.use_input_in_reset_gate,
                                        use_buffer_in_reset_gate=opt.use_buffer_in_reset_gate,
                                        buffer_short_size=opt.buffer_short_size,
                                        buffer_long_size=opt.buffer_long_size)
        else:
            self.rnn = getattr(
                nn,
                self.rnn_type.upper())(
                self.input_size,
                self.rnn_size,
                self.num_layers,
                bias=False,
                dropout=self.drop_prob_lm)

    def forward(self, xt, state):
        output, state = self.rnn(xt.unsqueeze(0), state)
        return output.squeeze(0), state


class MANet(nn.Module):
    """
    MANet: Modal Attention
    """

    def __init__(self, video_encoding_size, rnn_size, num_feats):
        super(MANet, self).__init__()
        self.video_encoding_size = video_encoding_size
        self.rnn_size = rnn_size
        self.num_feats = num_feats

        self.f_feat_m = nn.Linear(self.video_encoding_size, self.num_feats)
        self.f_h_m = nn.Linear(self.rnn_size, self.num_feats)
        self.align_m = nn.Linear(self.num_feats, self.num_feats)

    def forward(self, x, h):
        f_feat = self.f_feat_m(x)
        f_h = self.f_h_m(h.squeeze(0))  # assuming now num_layers is 1
        att_weight = nn.Softmax()(self.align_m(nn.Tanh()(f_feat + f_h)))
        att_weight = att_weight.unsqueeze(2).expand(
            x.size(0), self.num_feats, self.video_encoding_size / self.num_feats)
        att_weight = att_weight.contiguous().view(x.size(0), x.size(1))
        return x * att_weight


class CaptionModel(nn.Module):
    """
    A baseline captioning model
    """

    def __init__(self, opt):
        super(CaptionModel, self).__init__()
        self.vocab_size = opt.vocab_size
        self.input_encoding_size = opt.input_encoding_size
        self.rnn_type = opt.rnn_type
        self.rnn_size = opt.rnn_size
        self.num_layers = opt.num_layers
        self.buffer_mem_size = opt.buffer_mem_size
        self.buffer_short_size = opt.buffer_short_size
        self.buffer_long_size = opt.buffer_long_size
        self.drop_prob_lm = opt.drop_prob_lm
        self.seq_length = opt.seq_length
        self.feat_dims = opt.feat_dims
        self.num_feats = len(self.feat_dims)
        self.seq_per_img = opt.train_seq_per_img
        self.model_type = opt.model_type
        self.bos_index = 1  # index of the <bos> token
        self.ss_prob = 0
        self.mixer_from = 0
        self.video_as_initial_state = opt.video_as_initial_state
        
        self.embed = nn.Embedding(self.vocab_size, self.input_encoding_size)
        self.logit = nn.Linear(self.rnn_size, self.vocab_size)
        self.dropout = nn.Dropout(self.drop_prob_lm)

        self.init_weights()
        self.feat_pool = FeatPool(
            self.feat_dims,
            self.input_encoding_size, #self.rnn_size,
            self.drop_prob_lm)

        if opt.model_type == 'standard':
            self.feat_pool2 = nn.Sequential(
                nn.Linear(
                    self.input_encoding_size*self.num_feats,
                    self.rnn_size),
                nn.ReLU())#,
                #nn.Dropout(self.drop_prob_lm))

        self.feat_expander = FeatExpander(self.seq_per_img)

        self.video_encoding_size = self.num_feats * self.input_encoding_size#self.rnn_size
        opt.video_encoding_size = self.video_encoding_size
        self.core = RNNUnit(opt)

        if self.model_type == 'manet':
            self.manet = MANet(
                self.video_encoding_size,
                self.rnn_size,
                self.num_feats)

        self.gpus_num = len([int(g_ind) for g_ind in os.environ['CUDA_VISIBLE_DEVICES'].split(',')]) if 'CUDA_VISIBLE_DEVICES' in os.environ else 1
        print('gpus_num='+str(self.gpus_num))

        #self.video_dsc_std_meter = utils.AverageMeter()
        #self.otm1_dsc_std_meter = utils.AverageMeter()

    def set_ss_prob(self, p):
        self.ss_prob = p

    def set_mixer_from(self, t):
        """Set values of mixer_from 
        if mixer_from > 0 then start MIXER training
        i.e:
        from t = 0 -> t = mixer_from -1: use XE training
        from t = mixer_from -> end: use RL training
        """
        self.mixer_from = t
        
    def set_seq_per_img(self, x):
        self.seq_per_img = x
        self.feat_expander.set_n(x)

    def init_weights(self):
        initrange = 0.1
        self.embed.weight.data.uniform_(-initrange, initrange)
        self.logit.bias.data.fill_(0)
        self.logit.weight.data.uniform_(-initrange, initrange)

    def init_hidden(self, batch_size):
        weight = next(self.parameters()).data

        if self.rnn_type == 'lstm':
            return (
                Variable(
                    weight.new(
                        self.num_layers,
                        batch_size,
                        self.rnn_size).zero_()),
                Variable(
                    weight.new(
                        self.num_layers,
                        batch_size,
                        self.rnn_size).zero_()))

        elif self.rnn_type == 'buffer':
            return Variable(
                weight.new(
                    self.buffer_mem_size,
                    batch_size,
                    self.rnn_size).zero_())

        elif self.rnn_type == 'buffer_container':
            return (Variable(
                        weight.new(
                            self.buffer_short_size,
                            batch_size,
                            self.rnn_size).zero_()),
                    Variable(
                        weight.new(
                            self.buffer_long_size,
                            batch_size,
                            self.rnn_size).zero_()))
        else:
            return Variable(
                weight.new(
                    self.num_layers,
                    batch_size,
                    self.rnn_size).zero_())

    def set_video_as_initial_state(self, state, video_dsc):

        if self.rnn_type == 'lstm':
            return tuple([sub_state + video_dsc.unsqueeze(0) for sub_state in state])
        elif self.rnn_type == 'buffer':
            state[0] = video_dsc
            return state
        elif self.rnn_type == 'buffer_container':
            for sub_state in state:
                sub_state[0] = video_dsc
            return state
        else:
            return state + video_dsc.unsqueeze(0)

    def forward(self, feats, seq):
        seq = utils.squeeze_first_dims_into_batch_mul_captions_num_per_video(seq)

        fc_feats = self.feat_pool(feats)
        if self.model_type == 'standard':
            fc_feats = self.feat_pool2(fc_feats)
        fc_feats = self.feat_expander(fc_feats)

        batch_size = fc_feats.size(0)
        state = self.init_hidden(batch_size)
        if self.video_as_initial_state:
            state = self.set_video_as_initial_state(state, fc_feats)

        outputs = []
        sample_seq = []
        sample_logprobs = []

        # -- if <image feature> is input at the first step, use index -1
        # -- the <eos> token is not used for training
        start_i = -1 if (self.model_type == 'standard' and not self.video_as_initial_state) else 0
        end_i = seq.size(1) - 1

        for token_idx in range(start_i, end_i):
            if token_idx == -1:
                xt = fc_feats
            else:
                # token_idx = 0 corresponding to the <BOS> token
                # (already encoded in seq)

                if self.training and token_idx >= 1 and self.ss_prob > 0.0:
                    sample_prob = fc_feats.data.new(batch_size).uniform_(0, 1)
                    sample_mask = sample_prob < self.ss_prob
                    if sample_mask.sum() == 0:
                        it = seq[:, token_idx].clone()
                    else:
                        sample_ind = sample_mask.nonzero().view(-1)
                        it = seq[:, token_idx].data.clone()
                        # fetch prev distribution: shape Nx(M+1)
                        prob_prev = torch.exp(outputs[-1].data)
                        sample_ind_tokens = torch.multinomial(
                            prob_prev, 1).view(-1).index_select(0, sample_ind)
                        it.index_copy_(0, sample_ind, sample_ind_tokens)
                        it = Variable(it, requires_grad=False)
                elif self.training and self.mixer_from > 0 and token_idx >= self.mixer_from:
                    prob_prev = torch.exp(outputs[-1].data)
                    it = torch.multinomial(prob_prev, 1).view(-1)
                    it = Variable(it, requires_grad=False)
                else:
                    it = seq[:, token_idx].clone()

                if token_idx >= 1:
                    # store the seq and its logprobs
                    sample_seq.append(it.data)
                    logprobs = outputs[-1].gather(1, it.unsqueeze(1))
                    sample_logprobs.append(logprobs.view(-1))
                
                # break if all the sequences end, which requires EOS token = 0
                if it.data.sum() == 0:
                    if self.gpus_num == 1:
                        break
                xt = self.embed(it)

            if self.model_type == 'standard':
                output, state = self.core(xt, state)
            else:
                if self.model_type == 'manet':
                    fc_feats = self.manet(fc_feats, state[0])

                #self.video_dsc_std_meter.update(torch.std(fc_feats).data[0])
                #self.otm1_dsc_std_meter.update(torch.std(xt).data[0])
                output, state = self.core(torch.cat([xt, fc_feats], 1), state)
                
            if token_idx >= 0:
                output = F.log_softmax(self.logit(self.dropout(output)), dim=1)
                outputs.append(output)
                
        # only returns outputs of seq input
        # output size is: B x L x V (where L is truncated lengths
        # which are different for different batch)
        final_outputs = torch.cat([_.unsqueeze(1) for _ in outputs], 1)
        final_sample_seq = torch.cat([_.unsqueeze(1) for _ in sample_seq], 1)
        final_sample_logprobs = torch.cat([_.unsqueeze(1) for _ in sample_logprobs], 1)

        final_outputs, final_sample_seq, final_sample_logprobs = self.unsqueeze_model_results(final_outputs, final_sample_seq, final_sample_logprobs)
        return final_outputs, Variable(final_sample_seq), final_sample_logprobs

    def unsqueeze_model_results(self, final_outputs, final_sample_seq, final_sample_logprobs):
        _final_outputs = utils.unsqueeze_first_dim_into_batch_and_captions_num_per_video(final_outputs, self.seq_per_img)
        _final_sample_seq = utils.unsqueeze_first_dim_into_batch_and_captions_num_per_video(final_sample_seq, self.seq_per_img)
        _final_sample_logprobs = utils.unsqueeze_first_dim_into_batch_and_captions_num_per_video(final_sample_logprobs, self.seq_per_img)
        return _final_outputs, _final_sample_seq, _final_sample_logprobs

    def sample(self, feats, opt={}):
        sample_max = opt.get('sample_max', 1)
        beam_size = opt.get('beam_size', 1)
        temperature = opt.get('temperature', 1.0)
        expand_feat = opt.get('expand_feat', 0)

        if beam_size > 1:
            return self.sample_beam(feats, opt)

        fc_feats = self.feat_pool(feats)

        if self.model_type == 'standard':
            fc_feats = self.feat_pool2(fc_feats)
        if expand_feat == 1:
            fc_feats = self.feat_expander(fc_feats)
        batch_size = fc_feats.size(0)
        state = self.init_hidden(batch_size)

        if self.video_as_initial_state:
            state = self.set_video_as_initial_state(state, fc_feats)

        seq = []
        seqLogprobs = []

        unfinished = fc_feats.data.new(batch_size).fill_(1).byte()

        # -- if <image feature> is input at the first step, use index -1
        start_i = -1 if (self.model_type == 'standard' and not self.video_as_initial_state) else 0
        end_i = self.seq_length - 1

        for token_idx in range(start_i, end_i):
            if token_idx == -1:
                xt = fc_feats
            else:
                if token_idx == 0:  # input <bos>
                    it = fc_feats.data.new(
                        batch_size).long().fill_(self.bos_index)
                elif sample_max == 1:
                    # output here is a Tensor, because we don't use backprop
                    sampleLogprobs, it = torch.max(logprobs.data, 1)
                    it = it.view(-1).long()
                else:
                    if temperature == 1.0:
                        # fetch prev distribution: shape Nx(M+1)
                        prob_prev = torch.exp(logprobs.data).cpu()
                    else:
                        # scale logprobs by temperature
                        prob_prev = torch.exp(
                            torch.div(
                                logprobs.data,
                                temperature)).cpu()
                    #import pdb; pdb.set_trace()
                    it = torch.multinomial(prob_prev, 1).cuda()
                    # gather the logprobs at sampled positions
                    sampleLogprobs = logprobs.gather(
                        1, Variable(it, requires_grad=False))
                    # and flatten indices for downstream processing
                    it = it.view(-1).long()

                xt = self.embed(Variable(it, requires_grad=False))

            if token_idx >= 1:
                unfinished = unfinished * (it > 0)

                #
                it = it * unfinished.type_as(it)
                seq.append(it)
                seqLogprobs.append(sampleLogprobs.view(-1))

                # requires EOS token = 0
                if unfinished.sum() == 0:
                    break

            if self.model_type == 'standard':
                output, state = self.core(xt, state)
            else:
                if self.model_type == 'manet':
                    fc_feats = self.manet(fc_feats, state[0])
                output, state = self.core(torch.cat([xt, fc_feats], 1), state)

            logprobs = F.log_softmax(self.logit(output))

        return torch.cat([_.unsqueeze(1) for _ in seq], 1), torch.cat(
            [_.unsqueeze(1) for _ in seqLogprobs], 1)

    def sample_beam(self, feats, opt={}):
        """
        modified from https://github.com/ruotianluo/self-critical.pytorch
        """
        beam_size = opt.get('beam_size', 5)
        fc_feats = self.feat_pool(feats)
        if self.model_type == 'standard':
            fc_feats = self.feat_pool2(fc_feats)
        batch_size = fc_feats.size(0)

        seq = torch.LongTensor(self.seq_length, batch_size).zero_()
        seqLogprobs = torch.FloatTensor(self.seq_length, batch_size)
        # lets process every image independently for now, for simplicity
            
        self.done_beams = [[] for _ in range(batch_size)]
        for k in range(batch_size):
            state = self.init_hidden(beam_size)
            fc_feats_k = fc_feats[k].expand(
                beam_size, fc_feats[k].size(0))

            if self.video_as_initial_state:
                state = self.set_video_as_initial_state(state, fc_feats_k)

            beam_seq = torch.LongTensor(self.seq_length, beam_size).zero_()
            beam_seq_logprobs = torch.FloatTensor(
                self.seq_length, beam_size).zero_()
            # running sum of logprobs for each beam
            beam_logprobs_sum = torch.zeros(beam_size)

            # -- if <image feature> is input at the first step, use index -1
            start_i = -1 if (self.model_type == 'standard' and not self.video_as_initial_state) else 0
            end_i = self.seq_length - 1

            for token_idx in range(start_i, end_i):
                if token_idx == -1:
                    xt = fc_feats_k
                elif token_idx == 0:  # input <bos>
                    it = fc_feats.data.new(
                        beam_size).long().fill_(self.bos_index)
                    xt = self.embed(Variable(it, requires_grad=False))
                else:
                    """perform a beam merge. that is,
                    for every previous beam we now many new possibilities to branch out
                    we need to resort our beams to maintain the loop invariant of keeping
                    the top beam_size most likely sequences."""
                    logprobsf = logprobs.float()  # lets go to CPU for more efficiency in indexing operations
                    # sorted array of logprobs along each previous beam (last
                    # true = descending)
                    ys, ix = torch.sort(logprobsf, 1, True)
                    candidates = []
                    cols = min(beam_size, ys.size(1))
                    rows = beam_size
                    if token_idx == 1:  # at first time step only the first beam is active
                        rows = 1
                    for c in range(cols):
                        for q in range(rows):
                            # compute logprob of expanding beam q with word in
                            # (sorted) position c
                            local_logprob = ys[q, c]
                            candidate_logprob = beam_logprobs_sum[
                                q] + local_logprob
                            candidates.append({'c': ix.data[q, c], 'q': q, 'p': candidate_logprob.data[
                                              0], 'r': local_logprob.data[0]})
                    candidates = sorted(candidates, key=lambda x: -x['p'])

                    # construct new beams
                    state = [state] if (type(state) is Variable) else state
                    new_state = [_.clone() for _ in state]
                    if token_idx > 1:
                        # well need these as reference when we fork beams
                        # around
                        beam_seq_prev = beam_seq[:token_idx - 1].clone()
                        beam_seq_logprobs_prev = beam_seq_logprobs[
                            :token_idx - 1].clone()

                    for vix in range(beam_size):
                        v = candidates[vix]
                        # fork beam index q into index vix
                        if token_idx > 1:
                            beam_seq[
                                :token_idx - 1,
                                vix] = beam_seq_prev[
                                :,
                                v['q']]
                            beam_seq_logprobs[
                                :token_idx - 1,
                                vix] = beam_seq_logprobs_prev[
                                :,
                                v['q']]

                        # rearrange recurrent states
                        for state_ix in range(len(new_state)):
                            # copy over state in previous beam q to new beam at
                            # vix
                            new_state[state_ix][
                                :, vix] = state[state_ix][
                                :, v['q']]  # dimension one is time step

                        # append new end terminal at the end of this beam
                        # c'th word is the continuation
                        beam_seq[token_idx - 1, vix] = v['c']
                        beam_seq_logprobs[
                            token_idx - 1, vix] = v['r']  # the raw logprob here
                        # the new (sum) logprob along this beam
                        beam_logprobs_sum[vix] = v['p']

                        if v['c'] == 0 or token_idx == self.seq_length - 2:
                            # END token special case here, or we reached the end.
                            # add the beam to a set of done beams
                            if token_idx > 1: 
                                ppl = np.exp(-beam_logprobs_sum[vix] / (token_idx - 1))
                            else:
                                ppl = 10000
                            self.done_beams[k].append({'seq': beam_seq[:, vix].clone(),
                                                       'logps': beam_seq_logprobs[:, vix].clone(),
                                                       'p': beam_logprobs_sum[vix],
                                                       'ppl': ppl 
                                                       })

                    # encode as vectors
                    it = beam_seq[token_idx - 1]
                    xt = self.embed(Variable(it.cuda()))

                if token_idx >= 1:
                    state = new_state[0] if len(new_state)==1 else new_state

                if self.model_type == 'standard':
                    output, state = self.core(xt, state)
                else:
                    if self.model_type == 'manet':
                        fc_feats_k = self.manet(fc_feats_k, state[0])
                    output, state = self.core(
                        torch.cat([xt, fc_feats_k], 1), state)

                logprobs = F.log_softmax(self.logit(output), dim=1)

            
            #self.done_beams[k] = sorted(self.done_beams[k], key=lambda x: -x['p'])
            self.done_beams[k] = sorted(
                self.done_beams[k], key=lambda x: x['ppl'])
            
            # the first beam has highest cumulative score
            seq[:, k] = self.done_beams[k][0]['seq']
            seqLogprobs[:, k] = self.done_beams[k][0]['logps']
            
        return seq.transpose(0, 1), seqLogprobs.transpose(0, 1)
